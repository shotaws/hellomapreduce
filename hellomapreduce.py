# -*- coding:utf-8 -*-

'''
http://spark.apache.org/examples.html
'''
from __future__ import print_function

from pyspark import SparkContext

if __name__ == "__main__":
    sc = SparkContext(appName="PythonWordCountExample")
    # Load and parse the data
    text_file = sc.textFile("./text.txt")

    tffm = text_file.flatMap(lambda line: line.split(" ")) 
    
    print(tffm.collect())
    print("\n")

    tffmm = tffm.map(lambda word: (word, 1))

    print(tffmm.take(100))
    print("\n")

    counts = tffmm.reduceByKey(lambda a, b: a + b)    
    print(counts.take(100))
    print("\n")


    '''
    counts = text_file.flatMap(lambda line: line.split(" ")) \
             .map(lambda word: (word, 1)) \
             .reduceByKey(lambda a, b: a + b)
    '''

    # Save and load model
    try:
        counts.saveAsTextFile("./outputCount")
    except Exception, e:
        print('FileAlreadyExistsException')
